package com.kovansys.mvp.batch;

import java.io.IOException;
import java.util.ArrayList;
import java.util.List;

import org.springframework.batch.core.Job;
import org.springframework.batch.core.Step;
import org.springframework.batch.core.configuration.annotation.EnableBatchProcessing;
import org.springframework.batch.core.configuration.annotation.JobBuilderFactory;
import org.springframework.batch.core.configuration.annotation.StepBuilderFactory;
import org.springframework.batch.core.launch.support.RunIdIncrementer;
import org.springframework.batch.item.ItemProcessor;
import org.springframework.batch.item.ItemReader;
import org.springframework.batch.item.ItemWriter;
import org.springframework.batch.item.file.FlatFileItemReader;
import org.springframework.batch.item.file.FlatFileItemWriter;
import org.springframework.batch.item.file.mapping.BeanWrapperFieldSetMapper;
import org.springframework.batch.item.file.mapping.DefaultLineMapper;
import org.springframework.batch.item.file.transform.BeanWrapperFieldExtractor;
import org.springframework.batch.item.file.transform.DelimitedLineAggregator;
import org.springframework.batch.item.file.transform.DelimitedLineTokenizer;
import org.springframework.batch.item.support.ListItemReader;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;
import org.springframework.core.io.ClassPathResource;

import com.kovansys.mvp.entity.Rack;

@Configuration
@EnableBatchProcessing
public class BatchConfiguration {
	
	@Autowired
	private JobBuilderFactory jobs;
	
	@Autowired
	private StepBuilderFactory stepBuilderFactory;
	
	public static List<Rack> rackList = new ArrayList<Rack>();
	
	/**
	 * for read
	 * @return
	 */
    @Bean
    public ItemReader<Rack> reader() {
        FlatFileItemReader<Rack> reader = new FlatFileItemReader<Rack>();
        reader.setLinesToSkip(1);
        reader.setResource(new ClassPathResource("test.csv"));
        reader.setLineMapper(new DefaultLineMapper<Rack>() {{
            setLineTokenizer(new DelimitedLineTokenizer() {{
                setNames(new String[] { "rackId", "rackType", "rackDivisionRecipe" });
                setDelimiter(",");
            }});
            setFieldSetMapper(new BeanWrapperFieldSetMapper<Rack>() {{
                setTargetType(Rack.class);
            }});
        }});
        return reader;
    }

    @Bean
    public ItemProcessor<Rack, Rack> processor() {
        return new RackItemProcessor();
    }
    
    @Bean
    public Job importUserJob(){
    	return jobs.get("importUserJob")
    			.incrementer(new RunIdIncrementer())
    			.start(step1())
    			.build();
    }
    
    @Bean
    public Step step1(){
    	return stepBuilderFactory.get("step1")
    			.<Rack, Rack>chunk(1)
    			.reader(reader())
    			.processor(processor())
    			.build();
    }
    
    /**
     * for write
     * @return
     */    
   
    @Bean
    public ItemReader<Rack> listReader() {
    	
    	ListItemReader<Rack> reader = new ListItemReader<Rack>(rackList);
    	return reader;
    }
    
    @Bean
    public ItemWriter<Rack> testWriter(){
    	FlatFileItemWriter<Rack> writer = new FlatFileItemWriter<Rack>(); 
    	//writer.setEncoding("utf-8");
    	writer.setResource(new ClassPathResource("write.csv"));
    	System.out.println(System.getProperty("java.class.path"));
    	//writer.setShouldDeleteIfEmpty(true);
    	//writer.setShouldDeleteIfExists(false);
    	//writer.setAppendAllowed(true);
    	writer.setLineAggregator(new DelimitedLineAggregator<Rack>(){{
    		setDelimiter(",");
    		setFieldExtractor(new BeanWrapperFieldExtractor<Rack>(){{
    			setNames(new String[] { "rackId", "rackType", "rackDivisionRecipe" });
    		}});
    	}});
		
    	//writer.write(items);
    	return writer;
    }
    
    @Bean
    public ItemProcessor<Rack, Rack> writeProcessor() {
        return new RackItemWriteProcessor();
    }
    
    @Bean
    public Job writeToCSVJob(){
    	return jobs.get("writeToCSVJob")
    			.incrementer(new RunIdIncrementer())
    			.start(stepWrite())
    			.build();
    }
    
    @Bean
    Step stepWrite(){
		return stepBuilderFactory.get("stepWrite")
    			.<Rack, Rack>chunk(10)
    			.reader(listReader())  
    			.processor(writeProcessor())
    			.writer(testWriter())
    			.build();    	
    }       
    
//    @Bean
//    public ItemWriter<Rack> writer(DataSource dataSource) {
//        JdbcBatchItemWriter<Rack> writer = new JdbcBatchItemWriter<Rack>();
//        writer.setItemSqlParameterSourceProvider(new BeanPropertyItemSqlParameterSourceProvider<Rack>());
//        writer.setSql("INSERT INTO people (first_name, last_name) VALUES (:firstName, :lastName)");
//        writer.setDataSource(dataSource);
//        return writer;
//    }
    // end::readerwriterprocessor[]

    // tag::jobstep[]
//    @Bean
//    public Job importUserJob(JobBuilderFactory jobs, Step s1) {
//        return jobs.get("importUserJob")
//        		.preventRestart()
//                .incrementer(new RunIdIncrementer())
//                .flow(s1)
//                .end()
//                .build();
//    }
   

//    @Bean
//    public Step step1(StepBuilderFactory stepBuilderFactory, ItemReader<Rack> reader,
//            ItemWriter<Rack> writer, ItemProcessor<Rack, Rack> processor) {
//        return stepBuilderFactory.get("step1").startLimit(1)
//                .<Rack, Rack> chunk(10)
//                .reader(reader)
//                .processor(processor)
//               // .writer(writer)
//                .build();
//    }
    // end::jobstep[]



}