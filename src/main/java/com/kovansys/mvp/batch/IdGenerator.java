package com.kovansys.mvp.batch;


import java.util.ArrayList;
import java.util.List;
/**
 * 
 * @author mio
 *
 */
public class IdGenerator {
	/**
	 * generate id list
	 * @param base
	 * @param start
	 * @param end
	 * @return
	 */
	public static List<String> getId(String base, int start, int end){
		if(start > end) return null;
		List<String> idList = new ArrayList<String>(); 
		int maxSize = getNumSize(end) + 1;
		for(int i=start; i<=end; i++){
			int numSize = getNumSize(i);
			int zeroSize = maxSize-numSize;
			String zero = "";
			for(int j=0; j<zeroSize; j++){
				zero += "0";
			}
			String id = base + zero + i;
			idList.add(id);
		}
		return idList;		
	}
	/**
	 * get a number's size
	 * @param num
	 * @return
	 */
	private static int getNumSize(int num){
		int size = 1;
		while(num>=10){
			size++;
			num = num / 10;
		}
		return size;
	}
}
