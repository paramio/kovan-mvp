package com.kovansys.mvp.batch;

import org.springframework.batch.item.ItemProcessor;

import com.kovansys.mvp.entity.Rack;

public class RackItemWriteProcessor implements ItemProcessor<Rack, Rack> {

	@Override
	public Rack process(Rack rack) throws Exception {
		// TODO Auto-generated method stub
		System.out.println("["+rack.getRackId()+","+rack.getRackType()+","+rack.getRackDivisionRecipe()+"]");
//		Rack rack2 = new Rack();
//		rack2.setRackId(rack.getRackId());
//		rack2.setRackType(rack.getRackType());
//		rack2.setRackDivisionRecipe(rack.getRackDivisionRecipe());
		return rack;
	}

}
