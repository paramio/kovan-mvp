package com.kovansys.mvp.batch;

import org.springframework.batch.item.ItemProcessor;
import org.springframework.beans.factory.annotation.Autowired;

import com.kovansys.mvp.entity.Rack;
import com.kovansys.mvp.repository.RackRepository;

public class RackItemProcessor implements ItemProcessor<Rack, Rack> {
	
	@Autowired
	private RackRepository rackRepo;
	@Override
	public Rack process(Rack rack) throws Exception {
		// TODO Auto-generated method stub
		final String rackId = rack.getRackId().toUpperCase();
		final String rackType = rack.getRackType().toUpperCase();
		final String rackDivisionRecipe = rack.getRackDivisionRecipe().toUpperCase();
		Rack transformedRack = new Rack(rackId, rackType, rackDivisionRecipe);
		rackRepo.save(transformedRack);
		System.out.println("Converting (" + rack + ") into (" + transformedRack + ")");
		
		return transformedRack;
	}

}
