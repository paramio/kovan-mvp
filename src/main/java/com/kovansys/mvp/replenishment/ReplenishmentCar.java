package com.kovansys.mvp.replenishment;

import java.util.ArrayList;
import java.util.List;
import java.util.Random;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;

import com.kovansys.mvp.batch.IdGenerator;
import com.kovansys.mvp.common.PrintTools;
import com.kovansys.mvp.entity.Bin;
import com.kovansys.mvp.entity.Inventory;
import com.kovansys.mvp.entity.Rack;
import com.kovansys.mvp.entity.Sku;
import com.kovansys.mvp.repository.BinRepository;
import com.kovansys.mvp.repository.InventoryRepository;
import com.kovansys.mvp.repository.RackRepository;
import com.kovansys.mvp.repository.SkuRepository;

@Component
public class ReplenishmentCar {

	@Autowired
	private RackRepository rackRepo;
	
	@Autowired
	private BinRepository binRepo;
	
	@Autowired
	private SkuRepository skuRepo;
	
	@Autowired
	private InventoryRepository invRepo;
	
	/**
	 * build Ware house
	 */
	public void buildWarehouse() {
		PrintTools.println("开始生成 RACK, BIN, SKU ,INVENTORY......");
		long startTime=System.currentTimeMillis(); 
		// replenish				
		List<Rack> rackList = generateBaseRack(1, 100);
		List<Bin> binList = generateBaseBin(rackList, 20, 1, 2000);
		List<Sku> skuList = generateBaseSku(1, 1000);
		generateBaseInventory(binList, skuList);		
		long endTime=System.currentTimeMillis(); 
		PrintTools.println(String.format("生成完毕\n共用时间：%f s", (float)(endTime-startTime)/1000));
	}
	
	/**
	 * generate base racks repository
	 * 
	 * @param rackRepo
	 * @param start
	 * @param end
	 */
	public List<Rack> generateBaseRack(int start, int end) {
		List<String> rackIdList = IdGenerator.getId("rack", start, end);
		List<Rack> rackList = new ArrayList<Rack>();
		// int rackIdListSize = rackIdList.size();
		for (String rackId : rackIdList) {
			Rack rack = new Rack(rackId, null, null);			
			rackList.add(rack);
		}		
		return rackRepo.save(rackList);
	}

	public List<Bin> generateBaseBin(List<Rack> rackList,int binsPerRack, int start, int end) {
		
		if(start>end) return null;
		if(binsPerRack > end-start) return null;
		List<Bin> binList = new ArrayList<Bin>();
		List<String> binIdList = IdGenerator.getId("bin", start, end);
		for(Rack rack : rackList){
			if(binIdList.size() <= 0) break;
			if(binIdList.size() < binsPerRack){
				binsPerRack = binIdList.size();
			}
			for(int i=0; i<binsPerRack; i++){
				Bin bin = new Bin(binIdList.get(i), 0, 0, 0, 0, 0, 0, rack);
				//binRepo.save(bin);
				binList.add(bin);
				binIdList.set(i, "DELETEFLAG");
			}
			for(int i=0; i<binsPerRack; i++){
				binIdList.remove("DELETEFLAG");
			}
		}
		return binList;
	}
	
	public List<Sku> generateBaseSku(int start, int end){
		if(start>end) return null;
		List<Sku> skuList = new ArrayList<Sku>();
		List<String> skuIdList = IdGenerator.getId("sku", start, end);	
		for(String skuId : skuIdList){
			Sku sku = new Sku(skuId, null, null, 0, 0, 0, 0, true, null, 0, null);			
			skuList.add(sku);
		}
		return skuRepo.save(skuList);			
	}
	
	public void generateBaseInventory(List<Bin> binList, List<Sku> skuList){
		int maxSize = binList.size();
		List<Sku> randSkuList = generateRandomSku(skuList, maxSize);
		Random rand = new Random();
		for(int i=0; i<maxSize; i++){
			int size = rand.nextInt(50);
			Inventory inv = new Inventory(size, size, 0);
			Bin bin = binList.get(i);
			inv.setBin(bin);
			inv.setSku(randSkuList.get(i));
			bin.getInventory().add(inv);			
		}	
		binRepo.save(binList);
	}
	
	public List<Sku> generateRandomSku(List<Sku> skuList, int max){
		Random rand = new Random();
		List<Sku> randSkuList = new ArrayList<Sku>();
		int oldSkuSize = skuList.size();
		// get a loop times
		int limit = (int) Math.ceil(max/oldSkuSize);
		// get a max size of the list
		int maxSize = oldSkuSize < max ? oldSkuSize : max;
		for(int i=0; i<limit; i++){
			if(randSkuList.size()>=max) break;
			// copy list to a temp list
			List<Sku> tempList = new ArrayList<Sku>();
			tempList.addAll(skuList);			
			for(int j=0; j<maxSize; j++){
				if(randSkuList.size()>=max) break;
				int index = rand.nextInt(tempList.size());
				randSkuList.add(tempList.get(index));
				tempList.remove(index);
			}
			
		}
		
		return randSkuList;
	}

}
