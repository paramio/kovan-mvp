package com.kovansys.mvp.replenishment;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;

import com.kovansys.mvp.common.PrintTools;
import com.kovansys.mvp.repository.BinRepository;
import com.kovansys.mvp.repository.FlagRepository;
import com.kovansys.mvp.repository.InventoryRepository;
import com.kovansys.mvp.repository.OrderLineRepository;
import com.kovansys.mvp.repository.OrdersRepository;
import com.kovansys.mvp.repository.RackRepository;
import com.kovansys.mvp.repository.SkuRepository;

@Component
public class Destroyer {
	
	@Autowired
	private FlagRepository flagRepo;
	
	@Autowired
	private OrderLineRepository lineRepo;
	
	@Autowired
	private RackRepository rackRepo;
	
	@Autowired
	private BinRepository binRepo;
	
	@Autowired
	private SkuRepository skuRepo;
	
	@Autowired
	private InventoryRepository invRepo;
	
	@Autowired
	private OrdersRepository orderRepo;
	
	public Destroyer() {
		// TODO Auto-generated constructor stub
	}
	
	public void deleteAllInBatch(){
		PrintTools.println("开始删除已有全部数据......");
		long startTime=System.currentTimeMillis(); 
		// please delete in the sequence.
		// do not upset,or it will fail
		flagRepo.deleteAllInBatch();
		lineRepo.deleteAllInBatch();
		orderRepo.deleteAllInBatch();		
		invRepo.deleteAllInBatch();
		skuRepo.deleteAllInBatch();		
		binRepo.deleteAllInBatch();
		rackRepo.deleteAllInBatch();
		long endTime=System.currentTimeMillis(); 
		PrintTools.println(String.format("删除完毕\n共用时间：%f s", (float)(endTime-startTime)/1000));
	}

}
