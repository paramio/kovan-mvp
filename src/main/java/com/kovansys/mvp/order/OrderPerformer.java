package com.kovansys.mvp.order;

import java.util.Date;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;

import com.kovansys.mvp.repository.OrdersRepository;
import com.kovansys.mvp.repository.SkuRepository;

@Component
public class OrderPerformer {
	
	@Autowired
	OrderManager orderManager;
	
	@Autowired
	OrderGenerator orderGen;
	
	public void generateOrders(){
		long startTime=System.currentTimeMillis(); 
		orderGen.setIdStart(1);
		orderGen.setIdEnd(500);
		orderGen.setIdBase("order");
		orderGen.setCreateTime(new Date());		 
		orderGen.generateOrders();
		long endTime=System.currentTimeMillis(); 
		System.out.println(String.format("订单生成共用时间：%f s", (float)(endTime-startTime)/1000));
	}
	
	public void processOrders(){
		long startTime=System.currentTimeMillis(); 
		orderManager.processOrders();
		long endTime=System.currentTimeMillis(); 
		
		System.out.println(String.format("订单处理共用时间：%f s", (float)(endTime-startTime)/1000));
	}
	
}
