package com.kovansys.mvp.order;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;

import com.kovansys.mvp.common.PrintTools;
import com.kovansys.mvp.entity.Bin;
import com.kovansys.mvp.entity.Inventory;
import com.kovansys.mvp.entity.OrderLine;
import com.kovansys.mvp.entity.Orders;
import com.kovansys.mvp.entity.Sku;
import com.kovansys.mvp.repository.InventoryRepository;
import com.kovansys.mvp.repository.OrdersRepository;

@Component
public class OrderManager {	
	
	@Autowired
	private OrdersRepository orderRepo;
	
	@Autowired
	private InventoryRepository invRepo;
	
	public void processOrders(){
		List<Orders> orderList = orderRepo.findAll();
		int orderSize = orderList.size();
		int counter = 0;
		for(Orders order : orderList){
			if(processOrder(order)){
				counter++;
			}
		}
		PrintTools.println(String.format("结果：订单总数%d，完成订单数%d，未完成订单数%d。", orderSize, counter, orderSize-counter));
	}
	
	//@Transactional
	public boolean processOrder(Orders order){	
		boolean ProcessedFlag = false;
		String orderId = order.getOrderId();	
		List<OrderLine> orderLines = order.getOrderLines();
		List<Inventory> processedLineList = new ArrayList<Inventory>();
		for(OrderLine orderLine : orderLines){
			Sku sku = orderLine.getSku();
			String lineId = orderLine.getLineId();
			String skuId = sku.getSkuId();
			int quantity = orderLine.getQuantity();
			List<Inventory> inventoryList = sku.getInventory();
			List<Inventory> processedInventoryList;
			HashMap<String, String> selResults = checkInventory(quantity, skuId, lineId);
			if(selResults.containsKey("true")){
				if(!ProcessedFlag){
					PrintTools.println(String.format("\n\n%s订单完成:\n", orderId));
				}
				processedInventoryList = processInventory(inventoryList, quantity, skuId, lineId);
				if(processedInventoryList!= null){
					processedLineList.addAll(processedInventoryList);
				};
				ProcessedFlag = true;
			}
			else{
				PrintTools.println(String.format("\n\n%s订单未能完成:\n", orderId));
				PrintTools.println(selResults.get("false"));
				break;
			}
		}	
		if(ProcessedFlag){
			invRepo.save(processedLineList);
			return true;
		}
		return false;
	}
	public HashMap<String, String> checkInventory(int quantity, String skuId, String lineId){
		
		HashMap<String, String> results = new HashMap<String, String>();
		String successStr = "";
		String failedStr = "";		
		int totals = invRepo.sumAvailable(skuId);		
		if(totals >= quantity){
			results.put("true",	successStr);
		}else{
			failedStr = String.format("%s订单项库存不足，%sSKU仅剩余%d,需要%d件.", lineId, skuId, totals, quantity);
			results.put("false", failedStr);
		}
		return results;
	}
	
	public List<Inventory> processInventory(List<Inventory> inventoryList, int quantity, String skuId, String lineId){
		int fullFillSize = 0;
		int invListSize = inventoryList.size();
		boolean processOk = false;
		List<Inventory> processedInventoryList = new ArrayList<Inventory>();
		PrintTools.println(String.format("订单项：%s 数量：%d.", lineId, quantity));
		for(int i=0; i<invListSize; i++){
			fullFillSize += inventoryList.get(i).getAvailable();
			if(fullFillSize >= quantity){
				for(int j=0; j<i+1; j++){
					Inventory inv = inventoryList.get(j);
					Bin bin = inv.getBin();
					String binId = bin.getBinId();
					String rackId = bin.getRack().getRackId();
					int oneSize = inv.getAvailable();					
					if(oneSize <= quantity){
						if(oneSize == 0) continue;
						inv.setReserved(oneSize);
						//invRepo.setFixedReservedFor(oneSize, binId);						
						quantity -= oneSize;
						PrintTools.println(String.format("从%s号架子的%s箱子取出%d个%sSKU,满足订单项%s.", rackId, binId, oneSize, skuId, lineId));
						oneSize = 0;
						inv.setAvailable(oneSize);
						//invRepo.setFixedAvailableFor(oneSize, binId);		
						processedInventoryList.add(inv);
					}else {
						inv.setReserved(quantity);
						//invRepo.setFixedReservedFor(quantity, binId);
						oneSize -= quantity;
						inv.setAvailable(oneSize);
						//invRepo.setFixedAvailableFor(oneSize, binId);
						PrintTools.println(String.format("从%s号架子的%s箱子取出%d个%sSKU,满足订单项%s.", rackId, binId, quantity, skuId, lineId));
						processedInventoryList.add(inv);
					}
				}
				processOk = true;
				break;
			}else{
				processOk = false;
			}
		}
		if(processOk){
			return processedInventoryList;
			//invRepo.save(processedInventoryList);
		}		
		return null;
	}
	
}
