package com.kovansys.mvp.order;

import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Date;
import java.util.List;
import java.util.Random;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import com.kovansys.mvp.batch.IdGenerator;
import com.kovansys.mvp.entity.OrderLine;
import com.kovansys.mvp.entity.Orders;
import com.kovansys.mvp.entity.Sku;
import com.kovansys.mvp.repository.OrdersRepository;
import com.kovansys.mvp.repository.SkuRepository;

@Component
public class OrderGenerator {
	
	@Autowired
	private OrdersRepository orderRepo;

	@Autowired
	private SkuRepository skuRepo;
	
	public static SimpleDateFormat DATE_FORMAT = new SimpleDateFormat("yyyyMMddHHmmss");
	
	private int quantity;
	private Date createTime;
	private int idStart;
	private int idEnd;
	private String idBase;
	
	public int getQuantity() {
		return quantity;
	}

	public void setQuantity(int quantity) {
		this.quantity = quantity;
	}

	public Date getCreateTime() {
		return createTime;
	}

	public void setCreateTime(Date createTime) {
		this.createTime = createTime;
	}

	public int getIdStart() {
		return idStart;
	}

	public void setIdStart(int idStart) {
		this.idStart = idStart;
	}

	public int getIdEnd() {
		return idEnd;
	}

	public void setIdEnd(int idEnd) {
		this.idEnd = idEnd;
	}

	public String getIdBase() {
		return idBase;
	}

	public void setIdBase(String idBase) {
		this.idBase = idBase;
	}

	public OrderGenerator() {
		
	}
	
	public OrderGenerator(int quantity, Date createTime){

		this.quantity = quantity;
		this.createTime = createTime;
	}
	
	public OrderGenerator( int quantity, Date createTime, String idBase, int idStart, int idEnd){

		this.quantity = quantity;
		this.createTime = createTime;
		this.idBase = idBase;
		this.idStart = idStart;
		this.idEnd = idEnd;		
	}
	/**
	public List<Orders> generateOrders(){
		List<String> orderIdList = IdGenerator.getId(idBase, idStart, idEnd);
		List<Orders> ordersList = new ArrayList<Orders>();
		for(String orderId : orderIdList){
			Orders order = new  Orders(orderId, null, 0, null, null);		
			ordersList.add(order);
		}
		return ordersList;
	}

	public void  generateLines(OrdersRepository orderRepo, SkuRepository skuRepo){
		List<Orders> ordersList = generateOrders();
		List<Sku> skuList = skuRepo.findAll();
		Random rand = new Random();		
		for(Orders order : ordersList){
			SimpleDateFormat ft = new SimpleDateFormat("yyyyMMddHHmmss");
			String lineId = ft.format(new Date());
			OrderLine orderLine = new OrderLine(lineId, rand.nextInt(4)+1, null);
			orderLine.setOrder(order);
			orderLine.setSku(skuList.get(0));
			order.getLine().add(orderLine);
			orderRepo.save(order);
		
		}
	}
	*/
	
	public void generateOrders(){
		List<String> orderIdList = IdGenerator.getId(idBase, idStart, idEnd);
		List<Orders> generatedOrderList = new ArrayList<Orders>();
		List<Sku> allSkus = skuRepo.findAll();
		Random rand = new Random();				
		int orderMaxSize = idEnd-idStart+1; 
		System.out.println(String.format("%d orders to be generated...", orderMaxSize));
		System.out.println("Generating order number...");
		int flag = 1;
		for(String orderId : orderIdList){
			int orderLinesSize = rand.nextInt(19)+1;
			System.out.println(String.format("订单：%s 订单条数：%d", orderId, orderLinesSize));
			Orders order = new Orders(orderId, null, 0, null, null);
			
			List<Sku> orderSkuList = new ArrayList<Sku>();
			
			for(int ithLine=0; ithLine<orderLinesSize; ithLine++){				
				Sku sku = null;
				do {
					sku = allSkus.get(rand.nextInt(allSkus.size()));
				}
				while (orderSkuList.contains(sku));
				orderSkuList.add(sku);
				
				String lineId = orderId + "_" + ithLine;
				OrderLine orderLine = new OrderLine(lineId, sku, rand.nextInt(4)+1, null);

				order.addLine(orderLine);
				
				System.out.println(String.format("订单项%d：%s", ithLine+1, orderLine.toString()));
			}	
			generatedOrderList.add(order);
			System.out.println(String.format("剩余订单数：%d", orderMaxSize-flag));
			flag++;
			
		}
		orderRepo.save(generatedOrderList);
		orderRepo.flush();

		System.out.println("所有订单已经生成完毕！");
	}
}
