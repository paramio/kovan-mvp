package com.kovansys.mvp.common;

public enum Dimension {
	LENGTH,
	WIDTH,
	HEIGHT,
	NONE,
}
