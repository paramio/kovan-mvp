package com.kovansys.mvp.entity;

import java.io.Serializable;

import javax.persistence.Embeddable;
import javax.persistence.ManyToOne;

@Embeddable
public class InventoryId implements Serializable {
	/**
	 * 
	 */
	private static final long serialVersionUID = 1L;

	@ManyToOne
	private Bin bin;

	@ManyToOne
	private Sku sku;

	public Bin getBin() {
		return this.bin;
	}

	public void setBin(Bin bin) {
		this.bin = bin;
	}

	public Sku getSku() {
		return this.sku;
	}

	public void setSku(Sku sku) {
		this.sku = sku;
	}

	public int hashCode() {
		final int PRIME = 31;
		int result = 1;
		result = (bin == null) ? 0 : bin.hashCode();
		result = PRIME * result + ((sku == null) ? 0 : sku.hashCode());
		return result;
	}

	public boolean equals(Object o) {
		if (this == o)
			return true;
		if (o == null || getClass() != o.getClass())
			return false;

		InventoryId that = (InventoryId) o;

		if (bin != null ? !bin.equals(that.bin) : that.bin != null)
			return false;
		if (sku != null ? !sku.equals(that.sku) : that.sku != null)
			return false;
		return true;
	}
}
