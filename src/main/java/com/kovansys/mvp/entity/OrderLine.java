package com.kovansys.mvp.entity;

import javax.persistence.CascadeType;
import javax.persistence.Entity;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToOne;
import javax.persistence.OneToOne;

@Entity
public class OrderLine {
	/**
	 * Unique identifier
	 */
	@Id
	private String lineId;
	
	/** The number of products this line requests
	 */
	private int quantity;	
	
	/**
	 * the order that this line belongs to
	 */
	@ManyToOne
	@JoinColumn(name = "orderId")
	private Orders order;
	
	/**
	 * The product this line requests
	 */
	@OneToOne(cascade = CascadeType.ALL)
	@JoinColumn(name = "skuId")
	private Sku sku;
	
	/**
	 * active/ paused/ canceled/ fulfilled/ partially fulfilled
	 */
	private String status;

	protected OrderLine(){}
	
	public OrderLine(String lineId, Sku sku,int quantity,  String status){
		this.lineId = lineId;
		this.sku = sku;
		this.quantity = quantity;
		this.status = status;
	}

	@Override
	public String toString() {
		// TODO Auto-generated method stub
		return String.format("[%s, %d, %s]", this.lineId, this.quantity, this.status);
	}
	
	
	public String getLineId() {
		return lineId;
	}
	
	public void setLineId(String lineId) {
		this.lineId = lineId;
	}
	
	public void setOrder(Orders order) {
		this.order = order;
		if (!order.getOrderLines().contains(this)) {
			order.getOrderLines().add(this);
		}
	}
	
	public Orders getOrder() {
		return order;
	}
	
	public Sku getSku(){
		return this.sku;
	}
	
	public void setSku(Sku sku){
		this.sku = sku;
	}
	
	public int getQuantity() {
		return quantity;
	}
	
	public void setQuantity(int quantity) {
		this.quantity = quantity;
	}
		
	
}
