package com.kovansys.mvp.entity;

import java.util.List;

import javax.persistence.CascadeType;
import javax.persistence.Entity;
import javax.persistence.FetchType;
import javax.persistence.Id;
import javax.persistence.OneToMany;


@Entity
public class Rack {
	
	@Id
	// @GeneratedValue(strategy = GenerationType.AUTO)
	/**
	 * Unique Identifyer
	 */
	private String rackId;
	/**
	 * Rack Type ID, null in MVP1.0
	 */
	private String rackType;
	/**
	 * Recipe ID, null in MVP1.0
	 */
	private String rackDivisionRecipe;
	
	@OneToMany(mappedBy = "rack", cascade=CascadeType.ALL, fetch = FetchType.LAZY)
	private List<Bin> bin;
	
	public String getRackId() {
		return rackId;
	}
	
	public void setRackId(String rackId) {
		this.rackId = rackId;
	}
	
	public String getRackType() {
		return rackType;
	}

	public void setRackType(String rackType) {
		this.rackType = rackType;
	}

	public String getRackDivisionRecipe() {
		return rackDivisionRecipe;
	}

	public void setRackDivisionRecipe(String rackDivisionRecipe) {
		this.rackDivisionRecipe = rackDivisionRecipe;
	}

	public Rack(){}
	public Rack(String rackId, String rackType, String rackDivisionRecipe){
		this.rackId = rackId;
		this.rackType = rackType;
		this.rackDivisionRecipe = rackDivisionRecipe;
	}
	@Override
    public String toString() {
        return String.format(
                "Rack[Rack Id=%s, Rack Division Recipe='%s']",
                rackId, rackDivisionRecipe);
    }
}
