package com.kovansys.mvp.entity;

import java.util.ArrayList;
import java.util.Date;
import java.util.List;
import java.util.Set;

import javax.persistence.CascadeType;
import javax.persistence.Entity;
import javax.persistence.FetchType;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.OneToMany;
import javax.persistence.Temporal;
import javax.persistence.TemporalType;
import javax.persistence.Transient;
import javax.transaction.Transactional;

@Entity
public class Orders {
	/**
	 * Unique Identifyer
	 */
	@Id
    // @GeneratedValue(strategy = GenerationType.AUTO)
	private String orderId;
	
	/**
	 * name, address…
	 */
	private String customerId;
	
	/**
	 * integer, assigned by algorithms and could change from time to time.
	 */
	private int priority;
	
	/**
	 * order created
	 */
	@Temporal(value=TemporalType.TIMESTAMP)
	@GeneratedValue(strategy = GenerationType.AUTO)
	private Date createdOn;
	
	/**
	 * Time when order's payment is processed
	 */
	@Temporal(value=TemporalType.TIMESTAMP)
	private Date processedOn;
	
	/**
	 * received/ assigned/ active/ paused/ canceled/ fulfilled/ partially fulfilled
	 */
	private String status;
	
	@OneToMany(mappedBy = "order", cascade=CascadeType.ALL, fetch = FetchType.EAGER)
	private List<OrderLine> orderLines = new ArrayList<OrderLine> ();

	public void addLine(OrderLine orderLine) {
		orderLines.add(orderLine);
		if (orderLine.getOrder() != this) {
			orderLine.setOrder(this);
		}
	}
	

	protected Orders(){}
	
	public Orders(String orderId, String customerId, int priority, Date processedOn, String status){
		this.orderId = orderId;
		this.customerId = customerId;
		this.priority = priority;
		this.processedOn = processedOn;
		this.status = status;
	}
	@Override
    public String toString() {
        return String.format(
                "Order[%s, %s, %d, %s, %s, %s]",
                orderId, customerId, priority, createdOn, processedOn, status);
    }
	
	
	
	public String getOrderId() {
		return orderId;
	}

	public void setOrderId(String orderId) {
		this.orderId = orderId;
	}

	public String getCustomerId() {
		return customerId;
	}

	public void setCustomerId(String customerId) {
		this.customerId = customerId;
	}

	public int getPriority() {
		return priority;
	}

	public void setPriority(int priority) {
		this.priority = priority;
	}

	public Date getCreatedOn() {
		return createdOn;
	}

	public void setCreatedOn(Date createdOn) {
		this.createdOn = createdOn;
	}

	public Date getProcessedOn() {
		return processedOn;
	}

	public void setProcessedOn(Date processedOn) {
		this.processedOn = processedOn;
	}

	public String getStatus() {
		return status;
	}

	public void setStatus(String status) {
		this.status = status;
	}
	
	public List<OrderLine> getOrderLines() {
		return orderLines;
	}
}
