package com.kovansys.mvp.entity;

import javax.persistence.EmbeddedId;
import javax.persistence.Entity;
import javax.persistence.Table;
import javax.persistence.Transient;

@Entity
@Table(name = "Sku_Bin")
public class Inventory {

	/**
	 * Unique identifier
	 */
	@EmbeddedId
	private InventoryId invId = new InventoryId();
	
	/**
	 * including multiple locations in the whole system, deducted when an pick task is received
	 */
	private int available;
	
	/**
	 * replenish task underway, quantity moves to available and physical inventory when replenish task is executed
	 */
	private int incoming;
	
	/**
	 * when a pick task is executed, the corresponding amount should be reserved
	 */
	private int reserved;

	// /**
	// * Bin ID
	// */
	// private String binLocation;
	protected Inventory() {
		
	}

	public Inventory(int available, int incoming, int reserved) {
		this.available = available;
		this.incoming = incoming;
		this.reserved = reserved;
	}

	public InventoryId getInventoryId() {
		return this.invId;
	}

	@Transient
	public Bin getBin() {
		return getInventoryId().getBin();
	}

	public void setBin(Bin Bin) {
		getInventoryId().setBin(Bin);
	}

	@Transient
	public Sku getSku() {
		return getInventoryId().getSku();
	}

	public void setSku(Sku sku) {
		getInventoryId().setSku(sku);
	}
	
	public int getAvailable() {
		return this.available;
	}
	
	public void setAvailable(int available) {
		this.available = available;
	}
	
	public int getIncoming() {
		return this.incoming;
	}
	
	public void setIncoming(int incoming) {
		this.incoming = incoming;
	}
	
	public int getReserved() {
		return this.reserved;
	}
	
	public void setReserved(int reserved) {
		this.reserved = reserved;
	}
	
	@Override
	public String toString() {
		// TODO Auto-generated method stub
		return String.format("[%s, %s, %s, %d, %d]",
				invId.getBin().getBinId(), invId.getSku().getSkuId(),
				this.available, this.incoming, this.reserved);
	}

}
