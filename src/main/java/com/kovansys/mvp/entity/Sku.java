package com.kovansys.mvp.entity;

import java.util.Date;
import java.util.List;

import javax.persistence.CascadeType;
import javax.persistence.Entity;
import javax.persistence.EnumType;
import javax.persistence.Enumerated;
import javax.persistence.FetchType;
import javax.persistence.Id;
import javax.persistence.OneToMany;
import javax.persistence.Temporal;
import javax.persistence.TemporalType;

import com.kovansys.mvp.common.Dimension;

@Entity
public class Sku {
	

	@Id
	// @GeneratedValue(strategy = GenerationType.AUTO)
	private String skuId;
	
	/**
	 * physical barcode on the product
	 */
	private String barcode;
	
	/**
	 * description words
	 */
	private String description;
	
	/**
	 * integer mm
	 */
	private int length;
	
	/**
	 * integer mm
	 */
	private int width;
	
	/**
	 * integer mm
	 */
	private int height;
	
	/**
	 * integer grams
	 */
	private int weight;
	
	/**
	 * yes/no
	 */
	private boolean keepVertical;
	
	/**
	 * length/width/height/none
	 */
    @Enumerated(EnumType.STRING)
	private Dimension nestedDimension;
    
	/**
	 * integer mm
	 */
	private int nestingIncrement;
	
	/**
	 * date
	 */
	@Temporal(value=TemporalType.TIMESTAMP)
	private Date expirationDate;
	
	
//	@JoinTable(name="Sku_Bins",
//			joinColumns= @JoinColumn(name = "Sku_ID", referencedColumnName = "skuId"),
//			inverseJoinColumns= @JoinColumn(name="Bin_ID",referencedColumnName = "binId")				
//			)
//	@ManyToMany
//	private List<Bins> bins;
	
	@OneToMany(mappedBy = "invId.sku", fetch = FetchType.EAGER)
	private List<Inventory> inventory;
	
	public List<Inventory> getInventory(){
		return this.inventory;		
	}
	
	public void setInventory(List<Inventory> inventory){
		this.inventory = inventory;
	}
	
	public void setSkuId(String skuId){
		this.skuId = skuId;		
	}
	
	public String getSkuId(){
		return this.skuId;		
	}
	
	protected Sku() {}
	
    public Sku(String skuId, String barcode, String description, int length, int width, int height, int weight, boolean keepVertical, Dimension nestedDimension, int nestingIncrement, Date expirationDate) {
        this.skuId = skuId;
    	this.barcode = barcode;
        this.description = description;
        this.length = length;
        this.weight = width;
        this.height = height;
        this.weight = weight;
    	this.keepVertical = keepVertical;
    	this.nestedDimension = nestedDimension;
    	this.nestingIncrement = nestingIncrement;
    	this.expirationDate = expirationDate;
    }
	
    
    @Override
    public String toString() {
        return String.format(
                "Sku[SKU Id=%s, UPC Code='%s', Description='%s']",
                skuId, barcode, description);
    }

	
		
}
