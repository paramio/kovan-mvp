package com.kovansys.mvp.entity;

import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;

@Entity
public class Flag {

	@Id

	private long flagId;
	
	private boolean flag;

	public boolean isFlag() {
		return flag;
	}

	public void setFlag(boolean flag) {
		this.flag = flag;
	}
	
	protected Flag(){}
	
	public Flag(long flagId, boolean flag){
		this.flagId = flagId;
		this.flag = flag;
	}
	
}
