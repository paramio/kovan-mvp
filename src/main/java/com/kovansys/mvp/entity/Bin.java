package com.kovansys.mvp.entity;

import java.util.ArrayList;
import java.util.List;

import javax.persistence.CascadeType;
import javax.persistence.Entity;
import javax.persistence.FetchType;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToOne;
import javax.persistence.OneToMany;

@Entity
public class Bin {
	
	/**
	 * Unique Identifyer
	 */
	@Id
	private String binId;
	/**
	 * The rack that this bin resides in
	 */
	@ManyToOne
	@JoinColumn(name = "rackId")
	private Rack rack;
	/**
	 * the bin's leftY bottom corner's X coordinate, integer mm, 
	 * using the rack's top leftY corner as 0,0.
	 */
	private int topX;
	/**
	 * the bin's leftY bottom corner's Y coordinate, integer mm, 
	 * using the rack's top leftY corner as 0,0.
	 */
	private int leftY;
	/**
	 * integer mm, net space (without the thickness of the divider)
	 */
	private int width;
	/**
	 * integer mm, net space (without the thickness of the divider)
	 */
	private int height;
	/**
	 * integer mm, net space (without the thickness of the divider)
	 */
	private int depth;
	/**
	 * integer mm
	 */
	private int lipHeight;
	
	@OneToMany(mappedBy = "invId.bin", cascade=CascadeType.ALL, fetch = FetchType.LAZY)
	private List<Inventory> inventory = new ArrayList<Inventory>();
	
	public List<Inventory> getInventory(){
		return this.inventory;		
	}
	
	public void setInventory(List<Inventory> inventory){
		this.inventory = inventory;		
	}
	
	public void setBinId(String binId){
		this.binId = binId;
	}
	
	public String getBinId(){
		return this.binId;
	}
//	@ManyToMany(mappedBy="bins")
//	private Set<Sku> sku;
	
	public Rack getRack() {
		return rack;
	}
	
	public void setRack(Rack rack) {
		this.rack = rack;
	}
	
	protected Bin(){}
	
	public Bin(String binId, int topX, int leftY, int width, int height, int depth, int lipHeight, Rack rack){
		this.binId = binId;
		this.topX = topX;
		this.leftY = leftY;
		this.width = width;
		this.height = height;
		this.depth = depth;
		this.lipHeight = lipHeight;
		this.rack = rack;
	}

	@Override
	public String toString() {
		// TODO Auto-generated method stub
		return this.binId;
	}
	
}
