package com.kovansys.mvp;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.CommandLineRunner;
import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;
import com.kovansys.mvp.order.OrderPerformer;
import com.kovansys.mvp.replenishment.Destroyer;
import com.kovansys.mvp.replenishment.ReplenishmentCar;
 
@SpringBootApplication
public class Application implements CommandLineRunner {
	
	@Autowired
	private ReplenishmentCar replCar;
	
	@Autowired
	private OrderPerformer orderPerformer;
	
	@Autowired
	private Destroyer destroyer;
	
	public static void main(String[] args) {
		
		SpringApplication.run(Application.class, args);
	}

	@Override
	public void run(String... arg0) throws Exception {
	
		 destroyer.deleteAllInBatch();
		 replCar.buildWarehouse();	
		 orderPerformer.generateOrders();
		 orderPerformer.processOrders();

	}
}
