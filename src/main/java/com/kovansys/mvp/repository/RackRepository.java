package com.kovansys.mvp.repository;

import org.springframework.data.jpa.repository.JpaRepository;

import com.kovansys.mvp.entity.Rack;

public interface RackRepository extends JpaRepository<Rack, Long> {

}
