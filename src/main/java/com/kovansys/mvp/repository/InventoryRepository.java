package com.kovansys.mvp.repository;

import javax.persistence.LockModeType;

import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.Lock;
import org.springframework.data.jpa.repository.Modifying;
import org.springframework.data.jpa.repository.Query;
import org.springframework.transaction.annotation.Transactional;

import com.kovansys.mvp.entity.Inventory;

public interface InventoryRepository extends JpaRepository<Inventory, Long>{
	
	@Lock(LockModeType.READ)
	@Transactional(readOnly = true)
	@Query("SELECT SUM(i.available) from #{#entityName} i where i.invId.sku.skuId = ?1")
	int sumAvailable(String skuId);
	
	/*
	@Modifying(clearAutomatically = true)
	@Transactional(readOnly = false)
	@Query("update #{#entityName} i set i.available = ?1 where i.invId.bin.binId = ?2")
	int setFixedAvailableFor(int available, String binId);
	*/

}
