package com.kovansys.mvp.repository;

import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.stereotype.Repository;

import com.kovansys.mvp.entity.Orders;


public interface OrdersRepository extends JpaRepository<Orders, Long> {
	Orders findByOrderId(String orderId);
}
