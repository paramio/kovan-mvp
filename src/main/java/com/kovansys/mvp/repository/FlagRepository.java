package com.kovansys.mvp.repository;


import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.stereotype.Repository;

import com.kovansys.mvp.entity.Flag;

public interface FlagRepository extends JpaRepository<Flag, Long> {

}
