package com.kovansys.mvp.repository;

import java.util.List;

import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.repository.query.Param;

import com.kovansys.mvp.entity.Bin;
import com.kovansys.mvp.entity.Sku;

public interface SkuRepository extends JpaRepository<Sku, Long> {
	
	// @Query("select b from Bins b join b.sku s where s.skuId = :skuid")	
	List<Bin> findBySkuId(@Param("skuid") String skuId);
	

}
