package com.kovansys.mvp.repository;

import org.springframework.data.jpa.repository.JpaRepository;
import com.kovansys.mvp.entity.OrderLine;

public interface OrderLineRepository extends JpaRepository<OrderLine, Long> {

}
